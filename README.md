# Overview #
This repository consists of all the documents related to TERN data licence policy and implementation framework. This repository is a point of reference to access latest version of the documents. 

### What is this repository for? ###
The repository host all the documents related to TERN data licence including policy and framework, data providers deed and TERN licence deeds. All the latest documents will be hosted in this repository including the immediate previous version.

### Contents of the repository ###
Following documents are available in this repository

* TERN data licencing framework 2.0
* TERN data licencing policy 3.0 which superseded data licensing policy 2.0
* TERN data providers deed v1.7 which superseded v1.6
* TERN licence review 2015
* TERN Attribution Data licence 1.0 deed
* TERN Attribution-Sharealike Data Licence v1.0 deed
* TERN Attribution-No Derivatives Data Licence v1.0 deed 


### Circulation guidelines ###
The documents can be downloaded and used by TERN partners, collaborators, data providers and data users. TERN director has authorised to release all the documents. All the documents are subjected to Creative Commons licence.

### Contact information ###
Please Contact esupport@tern.org.au for any further information.